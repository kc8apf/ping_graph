#!/usr/bin/env bash

set -u

PING=ping
RRDTOOL=rrdtool
AWK=gawk

COUNT=4
RRD_DIR=.
TIMEOUT=10
VERBOSE=0

function usage() {
  echo "Usage: `basename $0` [-c count] [-d rrd_dir] [-t timeout] [-v level] <host>"
  echo ""
  echo -e "-c count\t\tMinimum number of successful pings required"
  echo -e "-d rrd_dir\t\tDirectory where RRD files are stored"
  echo -e "-t timeout\t\tMaximum time in seconds to attempt pings"
  echo -e "-v level\t\tEnable verbose logging at level <level>"
  exit $1
}

function verbose_log() {
  if [ $VERBOSE -ge $1 ]; then
    echo $2;
  fi
}

while getopts "c:d:t:v:" Option
do
  case $Option in
    c )
      COUNT=$OPTARG
      ;;
    d )
      if [ ! -d "$OPTARG" ]; then
        echo "RRD directory does not exist: $OPTARG";
        usage -85;
      fi
      RRD_DIR=$OPTARG
      ;;
    t)
      TIMEOUT=$OPTARG
      ;;
    v)
      VERBOSE=$OPTARG
      ;;
    ?)
      echo "Unknown option: $Option"
      usage -85
      ;;
  esac
done
shift $(($OPTIND - 1)) 

if [ $# -ne 1 ]; then
    usage -85;
fi

HOST="$1"
RRD_FILE="${RRD_DIR}/${HOST}.rrd"

if [ ! -e ${RRD_FILE} ]; then
  verbose_log 1 "Creating RRD file for ${HOST}: ${RRD_FILE}"
  ${RRDTOOL} create ${RRD_FILE} \
    --step 300 \
    DS:pl:GAUGE:600:0:100 \
    DS:rtt:GAUGE:600:0:10000000 \
    RRA:AVERAGE:0.5:1:800 \
    RRA:AVERAGE:0.5:6:800 \
    RRA:AVERAGE:0.5:24:800 \
    RRA:AVERAGE:0.5:288:800 \
    RRA:MAX:0.5:1:800 \
    RRA:MAX:0.5:6:800 \
    RRA:MAX:0.5:24:800 \
    RRA:MAX:0.5:288:800
fi

verbose_log 1 "Pinging ${HOST} ${COUNT} times with a ${TIMEOUT} second timeout"
output=$(${PING} -q -n -c ${COUNT} -t ${TIMEOUT} ${HOST} 2>&1)
verbose_log 2 "Ping output: ${output}"
# notice $output is quoted to preserve newlines
results=$(echo "$output"| $AWK '
BEGIN {
    pl=100;
    rtt="inf"
}
/packets transmitted/ {
    match($0, /([0-9]+(\.[0-9]+)?)% packet loss/, matchstr)
    pl=matchstr[1]
}
/^rtt/ {
    # looking for something like 0.562/0.566/0.571/0.024
    match($4, /(.*)\/(.*)\/(.*)\/(.*)/, a)
    rtt=a[3]
}
/unknown host/  {
    # no output at all means network is probably down
    pl=100
    rtt="inf"
}
/temporary failure in name resolution/ {
    # DNS failure means network is probably down
    pl=100
    rtt="inf"
}
END             {print pl ":" rtt}
')

verbose_log 1 "Updating RRD ${RRD_FILE} with results: ${results}"
$RRDTOOL update ${RRD_FILE} \
  --template pl:rtt N:$results
